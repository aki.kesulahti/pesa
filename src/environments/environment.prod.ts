export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyAPY7PLlthq2FUIVIDtvQzCphB83w2cHCY',
    authDomain: 'proplistings-2d863.firebaseapp.com',
    databaseURL: 'https://proplistings-2d863.firebaseio.com',
    projectId: 'proplistings-2d863',
    storageBucket: 'proplistings-2d863.appspot.com',
    messagingSenderId: '655778233743'
  }
};
