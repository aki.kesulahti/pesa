// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAPY7PLlthq2FUIVIDtvQzCphB83w2cHCY',
    authDomain: 'proplistings-2d863.firebaseapp.com',
    databaseURL: 'https://proplistings-2d863.firebaseio.com',
    projectId: 'proplistings-2d863',
    storageBucket: 'proplistings-2d863.appspot.com',
    messagingSenderId: '655778233743'
  }
};

