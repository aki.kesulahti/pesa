import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  user: Observable<firebase.User>;
  username: string;
  avatar: string;

  constructor(private authService:AuthService,  public flashMessage:FlashMessagesService) {
    this.user = authService.user;
    this.user.subscribe(data => {
      if (data) {
        this.username = data.displayName;
        this.avatar = data.photoURL;
      }
    });
    
  }

  login() {
    this.authService.login();
  }

  logout() {
    this.authService.logout();
  }

  ngOnInit() {

  }

   testMethod() {
    this.flashMessage.show(
      'Pesän välimuisti on tyhjennetty',
      {cssClass: 'alert-warning', timeout: 3000}
      );
  }


}
