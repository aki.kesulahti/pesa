import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { GeolocationService } from '../../services/geolocation.service';


@Component({
  selector: 'app-add-listing',
  templateUrl: './add-listing.component.html',
  styleUrls: ['./add-listing.component.css']
})
export class AddListingComponent implements OnInit {

  types = ['Siitoshirvas', 'Siitosvaadin', 'Siitosvasa', 'Teurashirvas', 'Teurasvaadin', 'Jne'];

  model = new Listing('', '', '', null, null, '', null);

  submitted = false;

  onSubmit() { this.submitted = true; }

  // Vaihdettu formin validointia luokkakohtaiseksi
  // title:any;
  // city:any;
  // owner:any;
  // bedrooms:any;
  // type:any;
  // price:any;
  
  image:any;

  constructor(
    private firebaseService:FirebaseService,
    private router:Router,
    private geolocationService:GeolocationService,
    public flashMessage:FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onAddSubmit() {
    let listing = {
      title: this.model.title,
      city: this.model.city,
      owner: this.model.owner,
      bedrooms: Number([String(this.model.bedrooms).slice(0, 2), '.', String(this.model.bedrooms).slice(2)].join('')),
      bedrooms2: Number([String(this.model.bedrooms2).slice(0, 2), '.', String(this.model.bedrooms2).slice(2)].join('')),
      type: this.model.type,
      price: this.model.price
      // Vaihdettu formin validointia luokkakohtaiseksi
      // title: this.title,
      // city: this.city,
      // owner: this.owner,
      // bedrooms: this.bedrooms,
      // type: this.type,
      // price: this.price
    }

    this.firebaseService.addListing(listing);

    this.router.navigate(['/listings']);
  }

  validate(value)  {
      (value < 0 || value % 1 != 0) ?
      (this.model.bedrooms = 0,
      this.flashMessage.show(
      'Numeron pitää ola positiivinen kokonaisluku',
      {cssClass: 'alert-warning', timeout: 3000}
      ))
      : this.model.bedrooms = value;
    }

  validate2(value)  {
      (value < 0 || value % 1 != 0) ?
      (this.model.bedrooms2 = 0,
      this.flashMessage.show(
      'Numeron pitää ola positiivinen kokonaisluku',
      {cssClass: 'alert-warning', timeout: 3000}
      ))
      : this.model.bedrooms2 = value;
    }


  validate3(value)  {
      (value < 0 || value % 1 != 0) ?
      (this.model.price = 0,
      this.flashMessage.show(
      'Numeron pitää ola positiivinen kokonaisluku',
      {cssClass: 'alert-warning', timeout: 3000}
      ))
      : this.model.price = value;
    }

}

export class Listing {
  constructor(
    public title: string,
    public city: string,
    public owner: string,
    public bedrooms: number,
    public bedrooms2: number,
    public type: string,
    public price: number
  ) {  }
}