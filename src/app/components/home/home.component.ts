import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: Observable<firebase.User>;

  lat: number = 60.195025;
  lng: number = 24.948251;

  constructor(
    public authService: AuthService
  ) { this.user = authService.user; }

  ngOnInit() {
  }

  login() {
    this.authService.login();
  }

}
