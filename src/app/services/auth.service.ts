import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Injectable()
export class AuthService {

  user: Observable<firebase.User>;

  constructor(
    public afAuth: AngularFireAuth,
    public flashMessage:FlashMessagesService,
    private router: Router,
    private route: ActivatedRoute
    ) {
      this.user = afAuth.authState;
  }

  login() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    this.router.navigate(['/']);
    this.flashMessage.show(
      'You are logged out',
      {cssClass: 'alert-success', timeout: 3000}
    );
    this.afAuth.auth.signOut();
  }

}
